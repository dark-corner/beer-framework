# Beer Framework

Main library for BeeR, a framework for dynamic task allocation
and resource management

## Build

To compile beer, the follwing libraries must be installed in your system:

- ZeroMQ
- Protobuf

First setup submodules:

```sh
git submodule init
git submodule update --recursive
```

Build using cmake, specifying MANGO\_ROOT directory:

```sh
cmake -DCMAKE_BUILD_TYPE=Debug \
  -DMANGO_ROOT=/opt/mango \
  -DCMAKE_INSTALL_PREFIX=/opt/mango ..

make
```
