#ifndef BUFFER_H
#define BUFFER_H

#include <mutex>
#include <memory>

namespace beer {
namespace server {

class Buffer {
  public:
    typedef std::shared_ptr<Buffer> Ptr;

    explicit Buffer(const uint32_t id,
                    const size_t   size,
                    const std::string& app_id);
    virtual ~Buffer();

    inline uint32_t           GetId()   const noexcept { return m_id; }
    inline size_t             GetSize() const noexcept { return m_size; }
    inline const std::string& GetName() const noexcept { return m_name; }

    void Write(const std::string &data);
    void Read(std::string& data);

    Buffer(const Buffer&) = delete;
    Buffer(Buffer&&) = delete;

    Buffer& operator=(const Buffer&) = delete;
    Buffer& operator=(Buffer&&) = delete;

  private:
    uint32_t    m_id;
    size_t      m_size;
    std::string m_name;

    // shared memory data
    int        m_shm_fd;
    char*      m_shm_ptr;
    std::mutex m_mutex;
};

} /* server */
} /* beer */

#endif /* ifndef BUFFER_H */
