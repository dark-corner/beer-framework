#ifndef SERVER_WORKER_H
#define SERVER_WORKER_H

#include "data_manager.h"
#include "server_interface.h"

#include "zmq.hpp"

#include <atomic>

namespace beer {
namespace server {

/*! \brief Dealer handling requests coming from the router
 *
 * This class implements a Dealer in the Router-Dealer design
 * pattern provided by ZeroMQ. The router will handle incoming requests
 * and forward them to the worker.
 *
 */

class ServerWorker : public ResponseHandler {
  public:
    ServerWorker(
        zmq::context_t& ctx,
        zmq::socket_type sock_type,
        const DataManager::Ptr& data_manager);

    ~ServerWorker() = default;

    /*! Worker thread handling messages coming from the router
     */
    void Work();
    void InitalizeResponse() override;
    void SendResponse(ResponseOption option,
                      const std::string& msg) override;

    ServerWorker(const ServerWorker&) = delete;
    ServerWorker(ServerWorker&&) = delete;

    ServerWorker& operator=(const ServerWorker&) = delete;
    ServerWorker& operator=(ServerWorker&&) = delete;

  private:
    zmq::context_t& m_context; /*!< ZeroMQ context */
    zmq::socket_t   m_worker; /*!< ZeroMQ socket connecting to the main dealer */

    std::atomic<bool> m_quit; /*!< control value for the main loop */

    zmq::message_t m_identity; /*!< message used to identify client request */
    zmq::message_t m_request; /*!< message payload */

    DataManager::Ptr m_data_mgr;
};

} /* namespace server */
} /* namespace beer */

#endif /* ifndef SERVER_WORKER_H */
