#ifndef SHARED_H
#define SHARED_H

#include <cstdint>

namespace beer
{

extern const uint32_t default_port;
extern const uint32_t api_version;

} /* beer */

#endif /* ifndef SHARED_H */
