#ifndef PROTOBUF_INTERFACE_H
#define PROTOBUF_INTERFACE_H

#include "data_manager.h"
#include "messages.pb.h"
#include "server_interface.h"

namespace beer {
namespace server {

class ProtobufBackend : public RequestHandler {
  public:
    explicit ProtobufBackend(ResponseHandler* handler);
    ~ProtobufBackend() = default;

    void ParseMessage(DataManager* manager,
                      const void* data,
                      const size_t size) override;

    ProtobufBackend(const ProtobufBackend&) = delete;
    ProtobufBackend(ProtobufBackend&&) = delete;

    ProtobufBackend& operator=(const ProtobufBackend&) = delete;
    ProtobufBackend& operator=(ProtobufBackend&&) = delete;

  private:
    ResponseHandler* m_response_handler;

    BeerMessage m_request, m_response;
};

} /* server */
} /* beer */

#endif /* ifndef PROTOBUF_INTERFACE_H */
