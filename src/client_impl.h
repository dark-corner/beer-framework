#ifndef CLIENT_IMPL_H
#define CLIENT_IMPL_H

#include "client.h"
#include "client_interface.h"

#include <string>

#include "zmq.hpp"

namespace beer {
namespace client {

class Client::ClientImpl : public RequestHandler {
  public:
    ClientImpl(const std::string& app_id);
    ~ClientImpl() = default;

    void Connect(const std::string& host,
                 const uint32_t port);

    inline zmq::context_t& GetContext() { return m_context; }
    inline zmq::socket_t&  GetSocket()  { return m_socket; }
    inline const std::string& GetAppId() { return m_app_id; }

    BeerMessage CreateRequestMessage();
    void SendMessage(const std::string& buf) override;
    void ReceiveResponse(std::string& buf) override;

    void ThrowError(const beer::Error& e);
    void ThrowError(const char* reason);

    ClientImpl(const ClientImpl&) = delete;
    ClientImpl(ClientImpl&&) = delete;

    ClientImpl& operator=(const ClientImpl&) = delete;
    ClientImpl& operator=(ClientImpl&&) = delete;

  private:
    zmq::context_t& m_context;
    zmq::socket_t   m_socket;

    const std::string m_app_id;
};

} /* client */
} /* beer */

#endif /* ifndef CLIENT_IMPL_H */
