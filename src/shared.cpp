#include "shared.h"

namespace beer
{

const uint32_t default_port = 9876;
const uint32_t api_version = 1;

} /* beer */ 
