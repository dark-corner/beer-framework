#ifndef KERNEL_H
#define KERNEL_H

#include <memory>
#include <mutex>
#include <string>
#include <vector>

namespace beer {
namespace server {

class Kernel {
  public:
    typedef std::shared_ptr<Kernel> Ptr;

    explicit Kernel(const uint32_t id,
                    const std::string& app_id,
                    const std::string& executable_data);
    virtual ~Kernel() = default;

    Kernel(const Kernel&) = delete;
    Kernel(Kernel&&) = delete;

    Kernel& operator=(const Kernel&) = delete;
    Kernel& operator=(Kernel&&) = delete;

    void Run(const std::vector<std::string>& args);
    int  Wait();

    inline pid_t              GetPid()   const noexcept { return m_pid; }
    inline uint32_t           GetId()   const noexcept { return m_id; }
    inline const std::string& GetPath()  const noexcept { return m_path; }
    inline uint32_t           GetStartTime() const noexcept { return m_start_time; }
    inline uint32_t           GetEndTime()   const noexcept { return m_end_time; }

  private:
    void StartTimingCount();
    void StopTimingCount();

    uint32_t    m_id;
    pid_t       m_pid;
    std::mutex  m_mutex;
    std::string m_path;
    long        m_start_time;
    long        m_end_time;
};

} /* namespace server */
} /* namespace beer */

#endif /* ifndef KERNEL_H */
