#include "protobuf_server_iface.h"
#include "server_worker.h"

#include <spdlog/spdlog.h>

namespace beer {
namespace server {

ServerWorker::ServerWorker(
    zmq::context_t& ctx,
    zmq::socket_type sock_type,
    const DataManager::Ptr& data_manager)
  : m_context(ctx)
  , m_worker(m_context, sock_type)
  , m_quit(false)
  , m_data_mgr(data_manager)
{ }

void ServerWorker::Work() {
  m_worker.connect("inproc://backend");
  spdlog::info("Starting worker thread");

  while (!m_quit) {
    ProtobufBackend receiver(this);

    m_worker.recv(m_identity);
    m_worker.recv(m_request);

    receiver.ParseMessage(m_data_mgr.get(),
                          m_request.data(),
                          m_request.size());
  }
}

void ServerWorker::InitalizeResponse() {
  m_worker.send(m_identity, zmq::send_flags::sndmore);
}

void ServerWorker::SendResponse(
    ResponseOption option,
    const std::string& msg) {
  zmq::send_flags flags;
  if (option == ResponseOption::None) {
    flags = zmq::send_flags::none;
  } else {
    flags = zmq::send_flags::sndmore;
  }
  zmq::message_t response(msg.begin(), msg.end());
  m_worker.send(response, flags);
}

} /* server  */ 
} /* beer  */ 
