#include "buffer.h"

#include <cstring>

#include <unistd.h>
#include <sys/shm.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <spdlog/spdlog.h>

namespace beer {
namespace server {

bool operator==(const Buffer& b1, const Buffer& b2) {
  return b1.GetName() == b2.GetName();
}

Buffer::Buffer(const uint32_t id,
               const size_t   size,
               const std::string& app_id)
  : m_id(id)
  , m_size(size)
  , m_name(app_id + std::to_string(id))
  , m_shm_fd(0)
  , m_shm_ptr(nullptr)
{
  m_shm_fd = shm_open(m_name.c_str(), O_CREAT | O_RDWR, 0666);
  int status = ftruncate(m_shm_fd, (off_t)size);
  if (status < 0) {
    throw std::runtime_error("buffer.cpp: ftruncate error");
  }
  m_shm_ptr = (char*) mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_SHARED, m_shm_fd, 0);
  spdlog::debug("[buffer={0:d} Initialized shared memory at {1:p}",
               id, (void*)m_shm_ptr);
}

Buffer::~Buffer() {
  if (m_shm_ptr != nullptr) {
    munmap(m_shm_ptr, m_size);
  }
  close(m_shm_fd);
  shm_unlink(m_name.c_str());
  spdlog::debug("[buffer={}] Clearing object", m_id);
}

void Buffer::Write(const std::string &data) {
    std::lock_guard<std::mutex> lck(m_mutex);
    if (data.size() == 0) {
      throw std::runtime_error("buffer cannot have size 0");
    }
    if (m_shm_fd == -1) {
        throw std::runtime_error("invalid shm fd");
    }
    if (m_shm_ptr == MAP_FAILED) {
        throw std::runtime_error("mmap failed");
    }
    // copy buffer into shm
    std::memcpy(m_shm_ptr, data.c_str(), m_size);
}

void Buffer::Read(std::string &b) {
    std::lock_guard<std::mutex> lck(m_mutex);
    if (m_shm_fd == -1) {
        throw std::runtime_error("invalid shm fd");
    }
    if (m_shm_ptr == MAP_FAILED) {
        throw std::runtime_error("mmap failed");
    }
    b.insert(0, m_shm_ptr, m_size);
}

} /* server */ 
} /* beer */ 
