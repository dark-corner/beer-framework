#ifndef SERVER_INTERFACE_H
#define SERVER_INTERFACE_H

#include "data_manager.h"

#include <string>

namespace beer {
namespace server {

/*! Interface that server worker need to implement to sent
 *  responses back to the client
 */
class ResponseHandler {
  public:
    enum ResponseOption {
      None = 0,
      SendMore
    };

    /*! Start the response routine by sending the identity to the
     *  server router. This method has to be called before any
     *  response message is send with SendResponse
     */
    virtual void InitalizeResponse() = 0;

    /*! Send the serialized response to the client which started
     *  the request.
     *
     *  \param last_message Indicates if this response is the last one.
     *  Useful if the server needs to send multiple response messages
     *  \param msg The serialized message
     */
    virtual void SendResponse(ResponseOption option,
                              const std::string& msg) = 0;
};

class RequestHandler {
  public:
    virtual void ParseMessage(DataManager* manager,
                              const void* data,
                              const size_t size) = 0;
};

} /* namespace server */
} /* namespace beer */

#endif /* ifndef PARSER_INTERFACE_H */
