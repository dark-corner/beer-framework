#include "server_impl.h"
#include "server_worker.h"

#include <functional>
#include <thread>
#include <vector>
#include <utility>

#include <spdlog/spdlog.h>

namespace beer {
namespace server {

Server::ServerImpl::ServerImpl()
  noexcept
  : m_context()
  , m_backend(m_context, zmq::socket_type::dealer)
  , m_frontend(m_context, zmq::socket_type::router)
  , m_data_mgr(std::make_shared<DataManager>())
{ }

void Server::ServerImpl::Setup(const uint32_t port) {
  std::string addr("tcp://*:");
  addr += std::to_string(port);
  m_frontend.bind(addr);
  m_backend.bind("inproc://backend");
}

void Server::ServerImpl::StartListening() {
  zmq::proxy(m_frontend, m_backend);
}

Server::Server()
  : m_impl(new ServerImpl())
{ }

Server::~Server()
{ }

void Server::Run(
    uint32_t port,
    uint32_t n_of_workers) {
  typedef std::unique_ptr<ServerWorker> WorkerPtr;
  typedef std::unique_ptr<std::thread>  ThreadPtr;

  std::vector<ThreadPtr> pool;
  std::vector<WorkerPtr> workers;

  try {
    m_impl->Setup(port);
  } catch (std::exception& e) {
    spdlog::error("Error while binding zeromq components: {}", e.what());
    return;
  }

  for(unsigned int i = 0; i < n_of_workers; ++i) {
    auto worker = new ServerWorker(m_impl->GetContext(),
                                   zmq::socket_type::dealer,
                                   m_impl->GetDataManager());
    workers.emplace_back(worker);
  }
  for(unsigned int i = 0; i < n_of_workers; ++i) {
    auto worker_ptr = workers[i].get();
    pool.emplace_back(new std::thread([worker_ptr]() {
      worker_ptr->Work();
    }));
  }

  try {
    spdlog::info("Started listening on port {}", port);
    m_impl->StartListening();
  } catch (std::exception& e) {
    spdlog::error("Unexpected error occurred: {}", e.what());
  }
}

} /* namespace server */
} /* namespace beer */
