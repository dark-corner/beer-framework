#ifndef DATA_MANAGER_H
#define DATA_MANAGER_H

#include "kernel.h"
#include "buffer.h"

#include <map>
#include <mutex>
#include <string>

namespace beer {
namespace server {

class DataManager {
  public:
    typedef std::shared_ptr<DataManager> Ptr;

    DataManager() = default;
    virtual ~DataManager() = default;

    DataManager(const DataManager&) = delete;
    DataManager(DataManager&&) = delete;

    DataManager& operator=(const DataManager&) = delete;
    DataManager& operator=(DataManager&&) = delete;

    void ClearApp(const std::string& app_id);

    void AddBuffer(const std::string& app_id,
                   const uint32_t id,
                   const uint32_t size);
    Buffer::Ptr GetBuffer(const std::string& app_id,
                          const uint32_t id);
    Kernel::Ptr GetKernel(const std::string& app_id,
                          const uint32_t id);
    void AddKernel(const std::string& app_id,
                   const std::string& executable_data,
                   const uint32_t id);

  private:
    struct Application {
      typedef std::shared_ptr<Application> Ptr;

      std::map<uint32_t, Kernel::Ptr> kernels;
      std::map<uint32_t, Buffer::Ptr> buffers;
    };

    Application::Ptr GetApp(const std::string& app_id);

    std::mutex m_mutex;
    std::map<std::string, Application::Ptr> m_apps;
};

} /* server */ 
} /* beer */ 

#endif /* ifndef DATA_MANAGER_H */
