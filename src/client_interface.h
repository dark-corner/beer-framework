#ifndef CLIENT_INTERFACE_H
#define CLIENT_INTERFACE_H

#include "messages.pb.h"

#include "zmq.hpp"

#include <string>

namespace beer {
namespace client {

class RequestHandler {
  public:
    virtual void SendMessage(const std::string& buf) = 0;
    virtual void ReceiveResponse(std::string& response) = 0;
};

} /* namespace client */
} /* namespace beer */

#endif /* ifndef CLIENT_INTERFACE_H */
