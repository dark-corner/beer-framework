#ifndef SERVER_IMPL_H
#define SERVER_IMPL_H

#include "data_manager.h"
#include "server.h"

#include "zmq.hpp"

namespace beer {
namespace server {

class Server::ServerImpl {
  public:
    ServerImpl() noexcept;
    ~ServerImpl() = default;

    void Setup(const uint32_t port);
    void StartListening();

    inline zmq::context_t& GetContext() noexcept { return m_context; }
    inline zmq::socket_t&  GetBackend() noexcept { return m_backend; }
    inline zmq::socket_t&  GetFrontend() noexcept { return m_frontend; }
    inline DataManager::Ptr  GetDataManager() noexcept { return m_data_mgr; }

    ServerImpl(const ServerImpl&) = delete;
    ServerImpl(ServerImpl&&) = delete;

    ServerImpl& operator=(const ServerImpl&) = delete;
    ServerImpl& operator=(ServerImpl&&) = delete;

  private:
    zmq::context_t m_context;
    zmq::socket_t  m_backend;
    zmq::socket_t  m_frontend;

    DataManager::Ptr m_data_mgr;
};

} /* server */
} /* beer */

#endif /* ifndef SERVER_IMPL_H */
