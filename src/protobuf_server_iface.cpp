#include "protobuf_server_iface.h"

#include <spdlog/spdlog.h>

namespace beer {
namespace server {

ProtobufBackend::ProtobufBackend(
    ResponseHandler* handler)
  : m_response_handler(handler)
{ }

void ProtobufBackend::ParseMessage(DataManager* manager,
                                   const void* data,
                                   size_t message_size) {
  std::string serialized_response;

  try {
    m_request.ParseFromArray(data, static_cast<int>(message_size));
  } catch (std::runtime_error& e) {
    spdlog::error("Error while parsing message: {}", e.what());
  }

  // variables common to all cases
  auto app_id = m_request.app_id();

  switch (m_request.msg_contents_case()) {
    case BeerMessage::kRegisterBuffer:
      {
        try {
          auto id = m_request.registerbuffer().id();
          auto size = m_request.registerbuffer().size();

          spdlog::info("[app={}] Received RegisterBuffer request "
                       "[id={} size={}]",
                       app_id,
                       id,
                       size);
          manager->AddBuffer(app_id, id, size);
          m_response.mutable_registerbufferdone();
        } catch (std::exception& e) {
          spdlog::error("Unable to register buffer: {}", e.what());
          auto err = m_response.mutable_error();
          err->set_error_code(1);
          err->set_error_msg(e.what());
        }
      }
      break;
    case BeerMessage::kRegisterKernel:
      {
        try {
          auto id = m_request.registerkernel().id();
          auto executable_data = m_request.registerkernel().executable();

          spdlog::info("[app={}] Received RegisterKernel request id={}",
                       app_id,
                       id);
          manager->AddKernel(app_id, executable_data, id);
          m_response.mutable_registerkerneldone();
        } catch (std::exception& e) {
          spdlog::error("Unable to register kernel: {}", e.what());
          auto err = m_response.mutable_error();
          err->set_error_code(1);
          err->set_error_msg(e.what());
        }
      }
      break;
    case BeerMessage::kKernelWait:
      {
        try {
          auto id = m_request.kernelwait().id();
          auto kernel = manager->GetKernel(app_id, id);

          spdlog::info("[app={}] Received KernelWait request id={}",
                       app_id,
                       id);
          int status = kernel->Wait();
          if (status != 0) {
            spdlog::error("Kernel execution failed with status={}",
                          status);
            auto err = m_response.mutable_error();
            err->set_error_code(status);
            err->set_error_msg("Kernel execution failed");
            // clear resources
            manager->ClearApp(app_id);
          }
          m_response.mutable_kernelwaitdone();
        } catch (std::exception& e) {
          spdlog::error("Kernel wait failed: {}", e.what());
          auto err = m_response.mutable_error();
          err->set_error_code(1);
          err->set_error_msg(e.what());
        }
      }
      break;
    case BeerMessage::kKernelRun:
      {
        try {
          auto id = m_request.kernelrun().id();
          auto kernel = manager->GetKernel(app_id, id);

          spdlog::info("[app={}] Received KernelRun request id={}",
                       app_id,
                       id);
          auto& repeated = m_request.kernelrun().args();
          std::vector<std::string> arg_list(repeated.begin(),
                                            repeated.end());
          kernel->Run(arg_list);
          m_response.mutable_kernelrundone();
        } catch (std::exception& e) {
          spdlog::error("Kernel run failed: {}", e.what());
          auto err = m_response.mutable_error();
          err->set_error_code(1);
          err->set_error_msg(e.what());
        }
      }
      break;
    case BeerMessage::kGetKernelTime:
      {
        try {
          auto id = m_request.getkerneltime().id();
          auto kernel = manager->GetKernel(app_id, id);

          spdlog::info("[app={}] Received GetKernelTime request id={}",
                       app_id,
                       id);
          auto done = m_response.mutable_getkerneltimedone();
          done->set_start_time(kernel->GetStartTime());
          done->set_end_time(kernel->GetEndTime());
        } catch (std::exception& e) {
          spdlog::error("Failed to obtain kernel statistics: {}", e.what());
          auto err = m_response.mutable_error();
          err->set_error_code(1);
          err->set_error_msg(e.what());
        }
      }
      break;
    case BeerMessage::kDetach:
      {
        try {
          spdlog::info("[app={}] Received Detach request",
                       app_id);
          manager->ClearApp(app_id);
          m_response.mutable_detachdone();
        } catch (std::exception& e) {
          spdlog::error("Failed to clear application data: {}", e.what());
          auto err = m_response.mutable_error();
          err->set_error_code(1);
          err->set_error_msg(e.what());
        }
      }
      break;
    case BeerMessage::kBufferRead:
      {
        try {
          auto id = m_request.bufferread().id();
          auto buffer = manager->GetBuffer(app_id, id);

          spdlog::info("[app={}] Received BufferRead request id={}",
                       app_id,
                       id);
          auto done = m_response.mutable_bufferreaddone();
          auto mem_storage = done->mutable_data();
          buffer->Read(*mem_storage);
        } catch (std::exception& e) {
          spdlog::error("Unable to read from buffer: {}", e.what());
          auto err = m_response.mutable_error();
          err->set_error_code(1);
          err->set_error_msg(e.what());
        }
      }
      break;
    case BeerMessage::kBufferWrite:
      {
        try {
          auto id = m_request.bufferwrite().id();
          auto buffer = manager->GetBuffer(app_id, id);

          spdlog::info("[app={}] Received BufferWrite request id={}",
                       app_id,
                       id);
          buffer->Write(m_request.bufferwrite().data());
          m_response.mutable_bufferwritedone();
        } catch (std::exception& e) {
          spdlog::error("Unable to write to buffer: {}", e.what());
          auto err = m_response.mutable_error();
          err->set_error_code(1);
          err->set_error_msg(e.what());
        }
      }
      break;
    case BeerMessage::MSG_CONTENTS_NOT_SET:
    default:
      {
        spdlog::error("Empty msg_contents in request message");
      }
      break;
  }

  // response section
  m_response_handler->InitalizeResponse();
  serialized_response = m_response.SerializeAsString();
  m_response_handler->SendResponse(ResponseHandler::ResponseOption::None,
                                   serialized_response);
}

} /* server */
} /* beer */
