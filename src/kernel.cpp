#include "kernel.h"

#include <unistd.h>
#include <sys/wait.h>

// for chmod function
#include <sys/stat.h>

#include <fstream>
#include <stdexcept>
#include <chrono>

#include <spdlog/spdlog.h>

namespace beer {
namespace server {

const char* tmp_base_path = "/tmp/";

Kernel::Kernel(const uint32_t id,
               const std::string& app_id,
               const std::string& executable_data)
  : m_id(id)
  , m_path(tmp_base_path + app_id)
  , m_start_time(0)
  , m_end_time(0)
{
  // TODO: add timestamp to file name to differentiate separate
  // instances of the same app

  std::ofstream ofs(m_path, std::ios::binary|std::ios::out);
  if (!ofs.good()) {
    throw std::runtime_error("Invalid path provided");
  }
  ofs.write(executable_data.c_str(), executable_data.size());
  ofs.close();
  chmod(m_path.c_str(), S_IRWXU);
  spdlog::debug("Kernel saved as {}", m_path);
}

void Kernel::Run(const std::vector<std::string>& args) {
  std::lock_guard<std::mutex> lck(m_mutex);
  StartTimingCount();
  m_pid = fork();
  if (m_pid < 0) {
    throw std::runtime_error("fork error");
  } else if (m_pid == 0) {
    // convert vector of strings to array of C strings
    const char** arg_list = new const char*[args.size() + 2];

    arg_list[0] = m_path.c_str();
    unsigned int i = 1;
    for(const auto& str : args) {
      arg_list[i] = str.c_str();
      ++i;
    }
    // debug
    for (unsigned int i = 0; i < args.size() + 1; ++i) {
      spdlog::debug("CMD_ARGS: {}", arg_list[i]);
    }
    // set last element of the array to NULL as required
    // by the execvp API
    arg_list[i] = NULL;
    execvp(m_path.c_str(), const_cast<char* const*>(arg_list));
  }
}

int Kernel::Wait() {
  // FIXME: StopTimingCount here is not correct. Wait can be
  // called even after the kernel finished execution
  std::lock_guard<std::mutex> lck(m_mutex);
  int status;
  spdlog::debug("[kernel={}] attempt to call waitpid", m_id);
  waitpid(m_pid, &status, 0);
  spdlog::debug("[kernel={}] waitpid returned status {}", m_id, status);
  StopTimingCount();
  return status;
}

void Kernel::StartTimingCount() {
  using clock = std::chrono::high_resolution_clock;
  using millisecs = std::chrono::milliseconds;

  auto now = clock::now();
  auto now_ms = std::chrono::time_point_cast<millisecs>(now);
  m_start_time = now_ms.time_since_epoch().count();
  spdlog::debug("{}: start_time={}",
                m_path,
                m_start_time);
}

void Kernel::StopTimingCount() {
  using clock = std::chrono::high_resolution_clock;
  using millisecs = std::chrono::milliseconds;

  auto now = clock::now();
  auto now_ms = std::chrono::time_point_cast<millisecs>(now);
  m_end_time = now_ms.time_since_epoch().count();
  spdlog::debug("{}: end_time={}",
                  m_path,
                  m_end_time);
}

} /* namespace server */
} /* namespace beer */
