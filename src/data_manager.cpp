#include "data_manager.h"

#include <spdlog/spdlog.h>

namespace beer {
namespace server {

void DataManager::ClearApp(const std::string& app_id) {
  auto iterator = m_apps.find(app_id);
  if (iterator == m_apps.end()) {
    spdlog::warn("Attempting to delete an already deleted application context");
  } else {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_apps.erase(iterator);
    spdlog::debug("Deleted app {}", app_id);
  }
}

DataManager::Application::Ptr DataManager::GetApp(
    const std::string& app_id) {
  Application::Ptr app_ptr;
  try {
    app_ptr = m_apps.at(app_id);
  } catch (std::exception& e) {
    app_ptr = std::make_shared<Application>();
    m_apps.emplace(app_id, app_ptr);
  }
  return app_ptr;
}


void DataManager::AddBuffer(const std::string& app_id,
                            const uint32_t id,
                            const uint32_t size) {
  auto app = GetApp(app_id);
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    if (app->buffers.find(id) != app->buffers.end()) {
      spdlog::warn("[app={}] Buffer {} already initialized",
                   app_id, id);
      return;
    }
  }
  Buffer::Ptr buffer_ptr = std::make_shared<Buffer>(id,
                                                    size,
                                                    app_id);
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    app->buffers.emplace(id, buffer_ptr);
  }
}

void DataManager::AddKernel(const std::string& app_id,
                            const std::string& executable_data,
                            const uint32_t id) {
  auto app = GetApp(app_id);
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    if (app->kernels.find(id) != app->kernels.end()) {
      spdlog::warn("[app={}] Kernel {} already initialized",
                   app_id, id);
      return;
    }
  }
  Kernel::Ptr kernel_ptr = std::make_shared<Kernel>(
      id,
      app_id,
      executable_data);
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    app->kernels.emplace(id, kernel_ptr);
  }
}

Buffer::Ptr DataManager::GetBuffer(const std::string& app_id,
                                   const uint32_t id) {
  Buffer::Ptr buffer_ptr;
  auto app = GetApp(app_id);
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    buffer_ptr = app->buffers.at(id);
  }
  return buffer_ptr;
}

Kernel::Ptr DataManager::GetKernel(const std::string& app_id,
                                   const uint32_t id) {
  Kernel::Ptr kernel_ptr;
  auto app = GetApp(app_id);
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    kernel_ptr = app->kernels.at(id);
  }
  return kernel_ptr;
}

} /* server */
} /* beer */
