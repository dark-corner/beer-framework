#include "client_impl.h"
#include "messages.pb.h"
#include "shared.h"

#include <fstream>

namespace beer {
namespace client {

static zmq::context_t main_context;

Client::ClientImpl::ClientImpl(const std::string& app_id)
  : m_context(main_context)
  , m_socket(m_context, zmq::socket_type::dealer)
  , m_app_id(app_id)
{ }

void Client::ClientImpl::Connect(const std::string& host,
                         const uint32_t port) {
  std::string conn_str("tcp://");
  conn_str += host;
  conn_str += ":";
  conn_str += std::to_string(port);
  m_socket.connect(conn_str);
}

BeerMessage Client::ClientImpl::CreateRequestMessage() {
  BeerMessage msg;
  msg.set_app_id(m_app_id);
  msg.set_api_version(api_version);
  return msg;
}

void Client::ClientImpl::SendMessage(const std::string& buf) {
  zmq::message_t msg(buf.data(), buf.size());
  m_socket.send(msg, zmq::send_flags::none);
}

void Client::ClientImpl::ReceiveResponse(std::string& buf) {
  zmq::message_t msg;
  m_socket.recv(msg, zmq::recv_flags::none);
  std::string temp(static_cast<char*>(msg.data()),
                   msg.size());
  buf = std::move(temp);
}

void Client::ClientImpl::ThrowError(const beer::Error& e) {
  throw BeerError(e.error_msg().c_str());
}

void Client::ClientImpl::ThrowError(const char* reason) {
  throw BeerError(reason);
}

Client::Client(const std::string& app_id)
  : m_impl(new ClientImpl(app_id))
{ }

void Client::Connect(const std::string& host,
                     const uint32_t port) {
  m_impl->Connect(host, port);
}

void Client::Connect(const Details& d) {
  m_impl->Connect(d.ip_address, d.port);
}

void Client::RegisterBuffer(const uint32_t id,
                            const uint32_t size) {
  BeerMessage request = m_impl->CreateRequestMessage();
  BeerMessage response;
  std::string serialized_request, serialized_response;

  auto buf = request.mutable_registerbuffer();
  buf->set_id(id);
  buf->set_size(size);

  serialized_request = request.SerializeAsString();
  m_impl->SendMessage(serialized_request);
  m_impl->ReceiveResponse(serialized_response);
  response.ParseFromString(serialized_response);

  if (response.has_error()) {
    m_impl->ThrowError(response.error());
  }
  if (!response.has_registerbufferdone()) {
    m_impl->ThrowError("Client::RegisterBuffer: Wrong response received");
  }
}

void Client::RegisterKernel(const uint32_t id,
                            const std::string& executable_path) {
  // messages data
  BeerMessage request = m_impl->CreateRequestMessage();
  BeerMessage response;
  std::string serialized_request, serialized_response;

  // buffer data to store the executable
  std::string executable_data;
  size_t binary_size;

  // read executable into memory buffer
  std::ifstream ifs(executable_path, std::ios::binary|std::ios::in);
  if (!ifs.good()) {
    throw std::runtime_error("path provided is not a valid file");
  }
  ifs.seekg(0, std::ios::end);
  binary_size = ifs.tellg();
  executable_data.resize(binary_size);
  ifs.seekg(0, std::ios::beg);
  ifs.read(&executable_data[0], binary_size);
  ifs.close();

  auto kernel = request.mutable_registerkernel();
  kernel->set_executable(executable_data);
  kernel->set_id(id);

  serialized_request = request.SerializeAsString();
  m_impl->SendMessage(serialized_request);
  m_impl->ReceiveResponse(serialized_response);
  response.ParseFromString(serialized_response);

  if (response.has_error()) {
    m_impl->ThrowError(response.error());
  }
  if (!response.has_registerkerneldone()) {
    m_impl->ThrowError("Client::RegisterKernel: Wrong response received");
  }
}

void Client::KernelWait(const uint32_t id) {
  BeerMessage request = m_impl->CreateRequestMessage();
  BeerMessage response;
  std::string serialized_request, serialized_response;

  auto kernel = request.mutable_kernelwait();
  kernel->set_id(id);

  serialized_request = request.SerializeAsString();
  m_impl->SendMessage(serialized_request);
  m_impl->ReceiveResponse(serialized_response);
  response.ParseFromString(serialized_response);

  if (response.has_error()) {
    m_impl->ThrowError(response.error());
  }
  if (!response.has_kernelwaitdone()) {
    m_impl->ThrowError("Client::KernelWait: Wrong response received");
  }
}

void Client::KernelRun(const uint32_t id,
                       const std::vector<std::string>& args) {
  BeerMessage request = m_impl->CreateRequestMessage();
  BeerMessage response;
  std::string serialized_request, serialized_response;

  auto kernel = request.mutable_kernelrun();
  kernel->set_id(id);
  auto args_ptr = kernel->mutable_args();
  *args_ptr = {args.begin(), args.end()};

  serialized_request = request.SerializeAsString();
  m_impl->SendMessage(serialized_request);
  m_impl->ReceiveResponse(serialized_response);
  response.ParseFromString(serialized_response);

  if (response.has_error()) {
    m_impl->ThrowError(response.error());
  }
  if (!response.has_kernelrundone()) {
    m_impl->ThrowError("Client::KernelRun: Wrong response received");
  }
}

Client::TimePoints Client::GetExecutionTime(const uint32_t id) {
  BeerMessage request = m_impl->CreateRequestMessage();
  BeerMessage response;
  std::string serialized_request, serialized_response;

  Client::TimePoints timings{0, 0};

  auto kernel = request.mutable_getkerneltime();
  kernel->set_id(id);

  serialized_request = request.SerializeAsString();
  m_impl->SendMessage(serialized_request);
  m_impl->ReceiveResponse(serialized_response);
  response.ParseFromString(serialized_response);

  timings.start_time = response.getkerneltimedone().start_time();
  timings.end_time = response.getkerneltimedone().end_time();

  if (response.has_error()) {
    m_impl->ThrowError(response.error());
  }
  if (!response.has_getkerneltimedone()) {
    m_impl->ThrowError("Client::GetExecutionTime: Wrong response received");
  }
  return timings;
}

void Client::Detach() {
  BeerMessage request = m_impl->CreateRequestMessage();
  BeerMessage response;
  std::string serialized_request, serialized_response;

  request.mutable_detach();

  serialized_request = request.SerializeAsString();
  m_impl->SendMessage(serialized_request);
  m_impl->ReceiveResponse(serialized_response);
  response.ParseFromString(serialized_response);

  if (response.has_error()) {
    m_impl->ThrowError(response.error());
  }
  if (!response.has_detachdone()) {
    m_impl->ThrowError("Client::Detach: Wrong response received");
  }
}

void Client::ReadBuffer(const uint32_t id,
                        std::string& data) {
  BeerMessage request = m_impl->CreateRequestMessage();
  BeerMessage response;
  std::string serialized_request, serialized_response;

  auto buffer_req = request.mutable_bufferread();
  buffer_req->set_id(id);

  serialized_request = request.SerializeAsString();
  m_impl->SendMessage(serialized_request);
  m_impl->ReceiveResponse(serialized_response);
  response.ParseFromString(serialized_response);

  data = std::move(response.bufferreaddone().data());

  if (response.has_error()) {
    m_impl->ThrowError(response.error());
  }
  if (!response.has_bufferreaddone()) {
    m_impl->ThrowError("Client::ReadBuffer: Wrong response received");
  }
}

void Client::WriteBuffer(const uint32_t id,
                         const std::string& data) {
  BeerMessage request = m_impl->CreateRequestMessage();
  BeerMessage response;
  std::string serialized_request, serialized_response;

  auto buffer_req = request.mutable_bufferwrite();
  buffer_req->set_id(id);
  buffer_req->set_data(data);

  serialized_request = request.SerializeAsString();
  m_impl->SendMessage(serialized_request);
  m_impl->ReceiveResponse(serialized_response);
  response.ParseFromString(serialized_response);

  if (response.has_error()) {
    m_impl->ThrowError(response.error());
  }
  if (!response.has_bufferwritedone()) {
    m_impl->ThrowError("Client::WriteBuffer: Wrong response received");
  }
}

Client::~Client()
{ }

} /* client */
} /* beer */
