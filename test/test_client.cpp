#include "client.h"

#include <fstream>
#include <iostream>
#include <vector>

#define UNUSED(x) (void)(x)

namespace bc = beer::client;

const char*       app_name = "test_app";
const uint32_t    kernel_id = 1;
const uint32_t    buffer_id = 2;
const std::string buffer_data = "Hello WORLD";
std::vector<std::string> kernel_args{
  "/usr/bin/sleep",
  "2",
};

int main(int argc, char *argv[])
{
  UNUSED(argc);
  UNUSED(argv);

  bc::Client client(app_name);
  client.Connect("localhost", beer::default_port);

  // test register buffer
  client.RegisterBuffer(buffer_id, buffer_data.size());

  // test register kernel
  client.RegisterKernel(kernel_id, kernel_args[0]);

  // buffer write and read
  client.WriteBuffer(buffer_id, buffer_data);
  std::string returned_data;
  client.ReadBuffer(buffer_id, returned_data);
  std::cout << "Server returned: " << returned_data << std::endl;

  // run, wait and get timing info
  kernel_args.erase(kernel_args.begin());
  client.KernelRun(kernel_id, kernel_args);
  client.KernelWait(kernel_id);
  auto timings = client.GetExecutionTime(kernel_id);
  std::cout << "Execution time: "
    << "start_time=" << timings.start_time
    << " end_time=" << timings.end_time
    << " delta=" << timings.end_time - timings.start_time << "ms"
    << std::endl;
  client.Detach();

  return 0;
}
