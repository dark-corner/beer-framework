add_executable(test-client test_client.cpp)
target_link_libraries(test-client PRIVATE Beer::client)
target_compile_options(test-client PRIVATE -Wall -Werror -std=c++11)
target_include_directories(test-client
  PRIVATE
    ${PROJECT_SOURCE_DIR}/include
)
