#ifndef CLIENT_H
#define CLIENT_H

#include "common.h"

#include <string>
#include <vector>

namespace beer {
namespace client {


class Client {
  public:
    struct TimePoints {
      uint32_t start_time;
      uint32_t end_time;
    };

    struct Details {
      // TODO: add architectural details
      std::string ip_address;
      uint32_t port;
    };


    Client(const std::string& app_id);
    virtual ~Client();

    void Connect(const std::string& host,
                 const uint32_t port);

    void Connect(const Details& d);

    void RegisterBuffer(const uint32_t id,
                        const uint32_t size);
    void ReadBuffer(const uint32_t id,
                    std::string& data);
    void WriteBuffer(const uint32_t id,
                     const std::string& data);
    void RegisterKernel(const uint32_t id,
                        const std::string& executable_path);
    void KernelWait(const uint32_t id);
    void KernelRun(const uint32_t id,
                   const std::vector<std::string>& args);
    TimePoints GetExecutionTime(const uint32_t kernel_id);

    void Detach();

    Client(const Client&) = delete;
    Client(Client&&) = delete;

    Client& operator=(const Client&) = delete;
    Client& operator=(Client&&) = delete;

  private:
    class ClientImpl;

    std::unique_ptr<ClientImpl> m_impl;
};

} /* client */ 
} /* beer */ 

#endif /* ifndef */
