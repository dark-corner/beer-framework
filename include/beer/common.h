#ifndef COMMON_H
#define COMMON_H

#include <cstdint>
#include <memory>

namespace beer
{

extern const uint32_t default_port;

class BeerError : public std::exception {
  public:
    BeerError(const char* reason)
      : m_reason(reason)
    { }
    virtual ~BeerError() { }
    const char* what() const noexcept override {
      return m_reason;
    }
  private:
    const char* m_reason;
};

} /* beer */

#endif /* ifndef COMMON_H */
