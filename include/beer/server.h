#ifndef SERVER_TASK_H
#define SERVER_TASK_H

#include "common.h"

#include <thread>

namespace beer {
namespace server {

class Server {
  public:
    Server();
    virtual ~Server();

    void Run(uint32_t port,
             uint32_t n_of_workers = std::thread::hardware_concurrency());

    Server(const Server&) = delete;
    Server(Server&&) = delete;

    Server& operator=(const Server&) = delete;
    Server& operator=(Server&&) = delete;

  private:
    class ServerImpl;

    std::unique_ptr<ServerImpl> m_impl;
};

} /* namespace server */
} /* namespace beer */

#endif /* ifndef SERVER_TASK_H */
