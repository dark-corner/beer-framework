#include "beer/server.h"

#include <spdlog/spdlog.h>

#define UNUSED(x) (void)(x)

namespace bs = beer::server;

int main(int argc, char *argv[])
{
  UNUSED(argc);
  UNUSED(argv);

  spdlog::set_level(spdlog::level::debug);

  bs::Server server;
  server.Run(beer::default_port);
  return 0;
}
